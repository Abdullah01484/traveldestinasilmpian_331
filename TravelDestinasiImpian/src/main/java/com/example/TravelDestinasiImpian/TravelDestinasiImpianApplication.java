package com.example.TravelDestinasiImpian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelDestinasiImpianApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelDestinasiImpianApplication.class, args);
	}

}
