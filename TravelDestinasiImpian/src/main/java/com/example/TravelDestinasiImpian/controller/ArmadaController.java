package com.example.TravelDestinasiImpian.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("armada")
public class ArmadaController {

//    @GetMapping(value = "index")
//    public ModelAndView index(){
//        ModelAndView view = new ModelAndView("armada/index");
//        return view;
//    }

    @RequestMapping("")
    public String armada(){ return ("armada/armada"); }

    @RequestMapping("addarmada")
    public String addarmada(){ return "armada/addArmada";}

    @RequestMapping("editarmada/{id}")
    public String editArmada(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "armada/editarmada";
    }
    @RequestMapping("deletearmada/{id}")
    public String deleteArmada(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "armada/deletearmada";
    }
}
