package com.example.TravelDestinasiImpian.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("kota")
public class KotaController {

//    @GetMapping(value = "index")
//    public ModelAndView index(){
//        ModelAndView view = new ModelAndView("kota/index");
//        return view;
//    }

    @RequestMapping("")
    public String kota(){ return ("kota/kota"); }

    @RequestMapping("addKota")
    public String addkota(){ return "kota/addKota";}

    @RequestMapping("editkota/{id}")
    public String editKota(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "kota/editkota";
    }
    @RequestMapping("deletekota/{id}")
    public String deleteKota(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "kota/deletekota";
    }
}
