package com.example.TravelDestinasiImpian.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("pengemudi")
public class PengemudiController {

//    @GetMapping(value = "index")
//    public ModelAndView index(){
//        ModelAndView view = new ModelAndView("pengemudi/index");
//        return view;
//    }

    @RequestMapping("")
    public String pengemudi(){ return ("pengemudi/pengemudi"); }

    @RequestMapping("addPengemudi")
    public String addpengemudi(){ return "pengemudi/addPengemudi";}

    @RequestMapping("editpengemudi/{id}")
    public String editPengemudi(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "pengemudi/editpengemudi";
    }
    @RequestMapping("deletepengemudi/{id}")
    public String deletePengemudi(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "pengemudi/deletepengemudi";
    }
}
