package com.example.TravelDestinasiImpian.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("wisata")
public class WisataController {

//    @GetMapping(value = "index")
//    public ModelAndView index(){
//        ModelAndView view = new ModelAndView("wisata/index");
//        return view;
//    }

    @RequestMapping("")
    public String wisata(){ return ("wisata/wisata"); }

    @RequestMapping("addWisata")
    public String addwisata(){ return "wisata/addWisata";}

    @RequestMapping("editwisata/{id}")
    public String editWisata(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "wisata/editwisata";
    }
    @RequestMapping("deletewisata/{id}")
    public String deleteWisata(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "wisata/deletewisata";
    }
}
