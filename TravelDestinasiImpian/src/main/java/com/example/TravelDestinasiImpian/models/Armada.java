package com.example.TravelDestinasiImpian.models;

import javax.persistence.*;

@Entity
@Table(name = "armada")
public class Armada extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name = "nopol", nullable = false)
    private String nopol;

    @Column(name = "kapasitas", nullable = false)
    private String kapasitas;

    @Column(name = "status", nullable = false)
    private Boolean status;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNopol() {
        return nopol;
    }

    public void setNopol(String nopol) {
        this.nopol = nopol;
    }

    public String getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(String kapasitas) {
        this.kapasitas = kapasitas;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
