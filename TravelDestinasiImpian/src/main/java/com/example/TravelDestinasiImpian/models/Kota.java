package com.example.TravelDestinasiImpian.models;

import javax.persistence.*;

@Entity
@Table(name = "kota")
public class Kota extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name = "nm_kota", nullable = false)
    private String nm_kota;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNm_kota() {
        return nm_kota;
    }

    public void setNm_kota(String nm_kota) {
        this.nm_kota = nm_kota;
    }
}
