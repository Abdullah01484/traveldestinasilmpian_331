package com.example.TravelDestinasiImpian.models;

import javax.persistence.*;

@Entity
@Table(name = "order_detail")
public class OrderDetail extends CommonEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @ManyToOne
    @JoinColumn(name = "order_header_id", insertable = false, updatable = false)
    public OrderHeader OrderHeader;

    @Column(name = "order_header_id")
    private long OrderHeaderId;

    @Column(name = "nama_pemesan")
    private String NamaPemesan;

    @ManyToOne
    @JoinColumn(name = "kota_id", insertable = false, updatable = false)
    public Kota Kota;

    @Column(name = "kota_id")
    private long KotaId;

    @ManyToOne
    @JoinColumn(name = "wisata_id", insertable = false, updatable = false)
    public Wisata Wisata;

    @Column(name = "wisata_id")
    private long WisataId;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public com.example.TravelDestinasiImpian.models.OrderHeader getOrderHeader() {
        return OrderHeader;
    }

    public void setOrderHeader(com.example.TravelDestinasiImpian.models.OrderHeader orderHeader) {
        OrderHeader = orderHeader;
    }

    public long getOrderHeaderId() {
        return OrderHeaderId;
    }

    public void setOrderHeaderId(long orderHeaderId) {
        OrderHeaderId = orderHeaderId;
    }

    public String getNamaPemesan() {
        return NamaPemesan;
    }

    public void setNamaPemesan(String namaPemesan) {
        NamaPemesan = namaPemesan;
    }

    public com.example.TravelDestinasiImpian.models.Kota getKota() {
        return Kota;
    }

    public void setKota(com.example.TravelDestinasiImpian.models.Kota kota) {
        Kota = kota;
    }

    public long getKotaId() {
        return KotaId;
    }

    public void setKotaId(long kotaId) {
        KotaId = kotaId;
    }

    public com.example.TravelDestinasiImpian.models.Wisata getWisata() {
        return Wisata;
    }

    public void setWisata(com.example.TravelDestinasiImpian.models.Wisata wisata) {
        Wisata = wisata;
    }

    public long getWisataId() {
        return WisataId;
    }

    public void setWisataId(long wisataId) {
        WisataId = wisataId;
    }
}
