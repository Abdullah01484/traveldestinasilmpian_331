package com.example.TravelDestinasiImpian.models;

import javax.persistence.*;

@Entity
@Table(name = "order_header")
public class OrderHeader extends CommonEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long Id;

    @Column(name = "Reference")
    private String Reference;

    @Column(name = "total_harga")
    private int TotalHarga;

    @Column(name = "nama_penyewa")
    private String NamaPenyewa;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public int getTotalHarga() {
        return TotalHarga;
    }

    public void setTotalHarga(int totalHarga) {
        TotalHarga = totalHarga;
    }

    public String getNamaPenyewa() {
        return NamaPenyewa;
    }

    public void setNamaPenyewa(String namaPenyewa) {
        NamaPenyewa = namaPenyewa;
    }
}
