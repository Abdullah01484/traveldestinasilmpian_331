package com.example.TravelDestinasiImpian.models;

import javax.persistence.*;

@Entity
@Table(name = "pengemudi")
public class Pengemudi extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "no_pegawai",nullable = false)
    private long no_pegawai;

    @Column(name = "nm_pengemudi",nullable = false)
    private String nm_pengemudi;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getNo_pegawai() {
        return no_pegawai;
    }

    public void setNo_pegawai(long no_pegawai) {
        this.no_pegawai = no_pegawai;
    }

    public String getNm_pengemudi() {
        return nm_pengemudi;
    }

    public void setNm_pengemudi(String nm_pengemudi) {
        this.nm_pengemudi = nm_pengemudi;
    }
}
