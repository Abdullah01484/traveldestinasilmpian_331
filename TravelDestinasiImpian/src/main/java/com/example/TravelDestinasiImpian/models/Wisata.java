package com.example.TravelDestinasiImpian.models;

import javax.persistence.*;

@Entity
@Table(name = "wisata")
public class Wisata extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long Id;

    @Column(name = "nm_wisata", nullable = false)
    private String nm_wisata;

    @ManyToOne
    @JoinColumn(name = "kota", insertable = false, updatable = false)
    public Kota kota;

    @Column(name = "kota")
    private long kotaId;

    @Column(name = "harga")
    private long harga;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNm_wisata() {
        return nm_wisata;
    }

    public void setNm_wisata(String nm_wisata) {
        this.nm_wisata = nm_wisata;
    }

    public Kota getKota() {
        return kota;
    }

    public void setKota(Kota kota) {
        this.kota = kota;
    }

    public long getKotaId() {
        return kotaId;
    }

    public void setKotaId(long kotaId) {
        this.kotaId = kotaId;
    }

    public long getHarga() {
        return harga;
    }

    public void setHarga(long harga) {
        this.harga = harga;
    }

}