package com.example.TravelDestinasiImpian.repository;

import com.example.TravelDestinasiImpian.models.Armada;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoArmada extends JpaRepository<Armada, Long> {

//  mengurutkan dari awal
    @Query(value = "SELECT * FROM armada ORDER BY armada ASC", nativeQuery = true)
    List<Armada> FindAllArmadaOrderByASC();

    @Query(value = "SELECT * FROM armada ORDER BY armada ASC", nativeQuery = true)
    Page<Armada> FindAllArmadaOrderByASC(Pageable pageable);

//  pencarian
    @Query("FROM Armada WHERE lower(nopol) LIKE lower(concat('%',?1,'%') ) ")
    List<Armada> SearchArmada(String keyword);
}
