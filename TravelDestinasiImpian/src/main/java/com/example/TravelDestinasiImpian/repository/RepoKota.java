package com.example.TravelDestinasiImpian.repository;

import com.example.TravelDestinasiImpian.models.Armada;
import com.example.TravelDestinasiImpian.models.Kota;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoKota extends JpaRepository<Kota, Long> {

    @Query(value = "SELECT * FROM kota ORDER BY kota ASC", nativeQuery = true)
    List<Kota> FindAllKotaOrderByASC();

    @Query(value = "SELECT * FROM kota ORDER BY kota ASC", nativeQuery = true)
    Page<Kota> FindAllKotaOrderByASC(Pageable pageable);

    @Query("FROM Kota WHERE lower(nm_kota) LIKE lower(concat('%',?1,'%') ) ")
    List<Kota> SearchKota(String keyword);
}
