package com.example.TravelDestinasiImpian.repository;

import com.example.TravelDestinasiImpian.models.Armada;
import com.example.TravelDestinasiImpian.models.OrderHeader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoOrderHeader extends JpaRepository<OrderHeader, Long> {

//  memilih id terbesar
    @Query("SELECT MAX(id) AS maxid FROM OrderHeader ")
    Long GetMaxOrderHeader();

    @Query(value = "SELECT * FROM order_header ORDER BY order_header ASC", nativeQuery = true)
    List<OrderHeader> FindAllOrderHeaderOrderByASC();

    @Query(value = "SELECT * FROM order_header ORDER BY order_header ASC", nativeQuery = true)
    Page<OrderHeader> FindAllOrderHeaderOrderByASC(Pageable pageable);

    @Query("FROM OrderHeader WHERE lower(Reference) LIKE lower(concat('%',?1,'%') ) ")
    List<OrderHeader> SearchOrderHeader(String keyword);
}
