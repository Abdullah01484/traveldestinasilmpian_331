package com.example.TravelDestinasiImpian.repository;

import com.example.TravelDestinasiImpian.models.Armada;
import com.example.TravelDestinasiImpian.models.Pengemudi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoPengemudi extends JpaRepository<Pengemudi, Long> {

    @Query(value = "SELECT * FROM pengemudi ORDER BY pengemudi ASC", nativeQuery = true)
    List<Pengemudi> FindAllPengemudiOrderByASC();

    @Query(value = "SELECT * FROM pengemudi ORDER BY pengemudi ASC", nativeQuery = true)
    Page<Pengemudi> FindAllPengemudiOrderByASC(Pageable pageable);

    @Query("FROM Pengemudi WHERE lower(nm_pengemudi) LIKE lower(concat('%',?1,'%') ) ")
    List<Pengemudi> SearchPengemudi(String keyword);
}
