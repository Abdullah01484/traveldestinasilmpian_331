package com.example.TravelDestinasiImpian.repository;

import com.example.TravelDestinasiImpian.models.Armada;
import com.example.TravelDestinasiImpian.models.Wisata;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoWisata extends JpaRepository<Wisata, Long> {

    @Query(value = "SELECT * FROM wisata ORDER BY wisata ASC", nativeQuery = true)
    List<Wisata> FindAllWisataOrderByASC();

    @Query(value = "SELECT * FROM wisata ORDER BY wisata ASC", nativeQuery = true)
    Page<Wisata> FindAllWisataOrderByASC(Pageable pageable);

    @Query("FROM Wisata WHERE lower(nm_wisata) LIKE lower(concat('%',?1,'%') ) ")
    List<Wisata> SearchWisata(String keyword);

    @Query(value = "SELECT * FROM wisata v WHERE v.kota = :kotaId", nativeQuery = true)
    List<Wisata> getWisataByKota(Long kotaId);
}
