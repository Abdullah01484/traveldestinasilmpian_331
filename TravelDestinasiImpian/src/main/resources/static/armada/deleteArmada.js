$(document).ready(function() {
	getArmadaById();

})

function getArmadaById() {
	var id = $("#delArmadaId").val();
	$.ajax({
		url: "/api/deletearmada/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#initDel").text(data.id);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delArmadaId").val();
	$.ajax({
		url : "/api/deletearmada/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				GetAllArmada();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})