$(document).ready(function() {
	getArmadaById();

})

function getArmadaById() {
	var id = $("#editArmadaId").val();
	$.ajax({
		url: "/api/getByIDArmada/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idArmInput").val(data.id);
			$("#nopInput").val(data.nopol);
			$("#kapInput").val(data.kapasitas);
            $("#statInput").val(data.status);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var idarmada = $("#idArmInput").val();
	var nomorpolisi = $("#nopInput").val();
	var muatan = $("#kapInput").val();
    var kondisi = $("#statInput").val();

	if (idarmada == "") {
		$("#errArm").text("Initial tidak boleh kosong!");
		return;
	} else {
		$("#errArm").text("");
	}
	if (nomorpolisi == "") {
		$("#errNop").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNop").text("");
	}
	if (muatan == "") {
    	$("#errKap").text("Initial tidak boleh kosong!");
    	return;
    } else {
    	$("#errKap").text("");
    }
    if (kondisi == "") {
    	$("#errStat").text("Name tidak boleh kosong!");
    	return;
    } else {
    	$("#errStat").text("");
    }

	var obj = {};
	obj.id = idarmada;
	obj.nopol = nomorpolisi;
	obj.kapasitas = muatan;
    obj.status = kondisi;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editarmada/" + idarmada,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllArmada();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
