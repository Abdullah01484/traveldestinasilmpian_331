$("#editCancelBtn").click(function(){
	$(".modal").modal("hide")
})

$("#editCreateBtn").click(function(){
	//var id = $("#initialInput").val();
	var kota = $("#kotaInput").val();

//	if(id == ""){
//		$("#errInitial").text("Initial tidak boleh kosong!");
//		return;
//	} else {
//		$("#errInitial").text("");
//	}
	if(kota == ""){
		$("#errKota").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errKota").text("");
	}
	var obj = {};
//	obj.id = id;
	obj.nm_kota = kota;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addkota",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){

				$(".modal").modal("hide")
				location.reload();
				getAllKota();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})