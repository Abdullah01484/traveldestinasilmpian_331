$(document).ready(function() {
	getKotaById();

})

function getKotaById() {
	var id = $("#delKotaId").val();
	$.ajax({
		url: "/api/deletekota/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#initDel").text(data.id);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delKotaId").val();
	$.ajax({
		url : "/api/deletekota/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				GetAllKota();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})