$(document).ready(function() {
	getKotaById();

})

function getKotaById() {
	var id = $("#editKotaId").val();
	$.ajax({
		url: "/api/getByIDKota/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idKotaInput").val(data.id);
			$("#kotaInput").val(data.nm_kota);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var idkota = $("#idKotaInput").val();
	var namakota = $("#kotaInput").val();

	if (idkota == "") {
		$("#errId").text("Initial tidak boleh kosong!");
		return;
	} else {
		$("#errId").text("");
	}
	if (namakota == "") {
		$("#errKota").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errKota").text("");
	}

	var obj = {};
	obj.id = idkota;
	obj.nm_kota = namakota;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editkota/" + idkota,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllKota();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
