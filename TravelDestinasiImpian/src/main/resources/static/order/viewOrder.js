$(document).ready(function() {
	getOrderDetailById();
})

function getOrderDetailById() {
	var id = $("#viewOrderDetail").val();
	$.ajax({
		url: "/api/getByIdOrderDetail/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    $("#reference").val(data.reference);
			$("#namaPemesan").val(data.namaPemesan);
            $("#hari").val(data.jmlHari);
            $("#price").val(data.harga);
		}
	})
}

$("#editviewBtnCancel").click(function() {
	$(".modal").modal("hide")
})