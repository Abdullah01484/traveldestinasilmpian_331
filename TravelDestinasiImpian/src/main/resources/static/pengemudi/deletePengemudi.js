$(document).ready(function() {
	getPengemudiById();

})

function getPengemudiById() {
	var id = $("#delPengemudiId").val();
	$.ajax({
		url: "/api/deletepengemudi/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#initDel").text(data.id);
		}
	})
}

$("#delCancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#delDeleteBtn").click(function() {
	var id = $("#delPengemudiId").val();
	$.ajax({
		url : "/api/deletepengemudi/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				GetAllPengemudi();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})