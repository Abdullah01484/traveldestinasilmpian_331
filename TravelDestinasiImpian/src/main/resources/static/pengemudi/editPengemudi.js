$(document).ready(function() {
	getPengemudiById();

})

function getPengemudiById() {
	var id = $("#editPengemudiId").val();
	$.ajax({
		url: "/api/getByIDPengemudi/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idInput").val(data.id);
			$("#nomorInput").val(data.no_pegawai);
			$("#namaInput").val(data.nm_pengemudi);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var idpengemudi = $("#idInput").val();
	var pegawai = $("#nomorInput").val();
	var pengemudi = $("#namaInput").val();

	if (idpengemudi == "") {
		$("#errId").text("Initial tidak boleh kosong!");
		return;
	} else {
		$("#errId").text("");
	}
	if (pegawai == "") {
		$("#errNom").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNom").text("");
	}
	if (pengemudi == "") {
    	$("#errNama").text("Name tidak boleh kosong!");
    	return;
    } else {
    	$("#errNama").text("");
    }

	var obj = {};
	obj.id = idpengemudi;
	obj.no_pegawai = pegawai;
	obj.nm_pengemudi = pengemudi;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editpengemudi/" + idpengemudi,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllPengemudi();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
