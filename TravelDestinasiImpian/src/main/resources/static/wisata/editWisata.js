$(document).ready(function() {
	getWisataById();

})

function getWisataById() {
	var id = $("#editWisataId").val();
	$.ajax({
		url: "/api/getByIDWisata/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#idInput").val(data.id);
			$("#kotaInput").val(data.kotaId);
			$("#namaInput").val(data.nm_wisata);
			$("#hargaInput").val(data.harga);
		}
	});
}

$("#editBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBtnCreate").click(function() {
	var idwisata = $("#idInput").val();
	var kota = $("#kotaInput").val();
	var wisata = $("#namaInput").val();
	var harga = $("#hargaInput").val();

	if (idwisata == "") {
		$("#errId").text("Initial tidak boleh kosong!");
		return;
	} else {
		$("#errId").text("");
	}
	if (kota == "") {
    	$("#errKota").text("Name tidak boleh kosong!");
    	return;
    } else {
    	$("#errKota").text("");
    }
	if (wisata == "") {
		$("#errNama").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNama").text("");
	}
    if (harga == "") {
        $("#errHarga").text("Name tidak boleh kosong!");
        return;
    } else {
        $("#errHarga").text("");
    }

	var obj = {};
	obj.id = idwisata;
	obj.kotaId = kota;
	obj.nm_wisata = wisata;
	obj.harga = harga;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editwisata/" + idwisata,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllWisata();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})
