function getAllWisata(){
	$("#wisataTable").html(
		`<thead>
			<tr>
				<th>IdWisata</th>
				<th>Kota</th>
				<th>NamaWisata</th>
				<th>Harga</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="wisataTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getAllWisata",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
//			console.log(data[i].id)
				$("#wisataTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${(data[i].kota).nm_kota}</td>
						<td>${data[i].nm_wisata}</td>
						<td>${data[i].harga}</td>
                        <td>
                        	<button value="${data[i].id}" onClick="editWisata(this.value)" class="btn btn-warning">
                        	<i class="bi-pencil-square"></i>
                        	</button>
                        <td>
                        <td>
                        	<button value="${data[i].id}" onClick="deleteWisata(this.value)" class="btn btn-danger">
                        	<i class="bi-trash"></i>
                        	</button>
                        </td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/wisata/addWisata",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Wisata");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editWisata(id){
	$.ajax({
		url: "/wisata/editwisata/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Wisata");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteWisata(id){
	$.ajax({
		url: "/wisata/deletewisata/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Wisata");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function WisataList(currentPage, length) {
    $.ajax({
		url : '/api/wisatamapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="WisataList(0,this.value)">'
//			table += '<option value="3" selected>..</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += "<table class='table table-bordered mt-3'>";
			table += "<tr> <th width='10%' class='text-center'>ID Wisata</th> <th>Kota</th> <th>Nama Wisata</th> <th>Harga</th> <th>Action</th></tr>"
			for (let i = 0; i < data.wisata.length; i++) {
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + (data.wisata[i].kota).nm_kota + "</td>";
				table += "<td>" + data.wisata[i].nm_wisata + "</td>";
				table += "<td>" + data.wisata[i].harga + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.wisata[i].id + "' onclick=editWisata(this.value)>Edit</button></td>";
				table += "<td><button class='btn btn-danger btn-sm' value='" + data.wisata[i].id + "' onclick=deleteWisata(this.value)>Delete</button></td>";
				// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="WisataList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="WisataList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="WisataList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#wisataList').html(table);
		}

	});
}

//function SearchWisata(request) {
////console.log(request)
//
//    if (request.length > 0)
//    {
//        $.ajax({
//        url: '/api/searchwisata/' + request,
//        type: 'GET',
//        contentType: 'application/json',
//        success: function (result) {
//                //console.log(result)
//                if (result.length > 0)
//                {
//                    for (let i = 0; i < result.length; i++) {
//                    $("#wisataTBody").html(
//                        `
//                        <tr>
//                            <td>${result[i].id}</td>
//                            <td>${(result[i].kota).nm_kota}</td>
//                            <td>${result[i].nm_wisata}</td>
//                            <td>${result[i].harga}</td>
//                            <td>
//                                 <button value="${result[i].id}" onClick="editWisata(this.value)" class="btn btn-warning">
//                                     <i class="bi-pencil-square"></i>
//                                 </button>
//                                 <button value="${result[i].id}" onClick="deleteWisata(this.value)" class="btn btn-danger">
//                                     <i class="bi-trash"></i>
//                                 </button>
//                            </td>
//                        </tr>
//                        `)
//                    }
//                }
//                else {
//                    $("#wisataTBody").html(
//                    `<tr>
//                        <td colspan='4' class='text-center'>No data</td>
//                    </tr>`
//                    );
//                }
//        },
//            error: function (error){
//                console.error('Error occurred', error);
//            }
//        });
//    }
//    else {
//	    getAllWisata();
//    }
//}

function SearchWisata(request) {
//console.log(request)

    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searchwisata/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    //console.log(result)
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='10%' class='text-center'>ID Wisata</th> <th>Nama Kota</th> <th>Nama Wisata</th> <th>Harga</th> <th>Action</th></tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
					    table += "<td class='text-center'>" + (i+1) + "</td>";
                        table += "<td>" + result[i].kota + "</td>";
                        table += "<td>" + result[i].nm_wisata + "</td>";
                        table += "<td>" + result[i].harga + "</td>";
                        table += "<td>" + result[i].Action + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editWisata(this.value)>Edit</button></td>";
                        table += "<td><button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deleteWisata(this.value)>Delete</button></td>";

						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='4' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#wisataList').html(table);
			}
		});
	} else {
        WisataList(0,5);
	}
}

$(document).ready(function(){
//	getAllWisata();
WisataList(0,5);
})